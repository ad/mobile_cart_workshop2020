{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Part 4: Topic Classification of Social Media <a class=\"tocSkip\"></a>  \n",
    "**Workshop: Social Media, Data Science, & Cartograpy**  \n",
    "_Alexander Dunkel, Madalina Gugulica_\n",
    "\n",
    "\n",
    "<img src=\"https://ad.vgiscience.org/mobile_cart_workshop2020/img_topics.png\" style=\"width:572px\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is the fourth notebook in a series of four notebooks:\n",
    "    \n",
    "1. Introduction to **Social Media data, jupyter and python spatial visualizations**\n",
    "2. Introduction to **privacy issues** with Social Media data **and possible solutions** for cartographers\n",
    "3. Specific visualization techniques example: **TagMaps clustering**\n",
    "4. Specific data analysis: **Topic Classification**\n",
    "\n",
    "Open these notebooks through the file explorer on the left side."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\" role=\"alert\" style=\"color: black;\">\n",
    "<p>The task in this notebook is very similar to the one presented in the previous notebook <em>Part3 - Tag Maps Clustering and Topic Heat Maps</em>, namely to explore the latent topics from LBSM data and visualize the textual information and its spatial distribution for a given topic. The emphasis in this notebook lies, however, on data exploration and analysis rather than on data visualization and focuses on a method of classifying social media posts based on the textual metadata. </p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**For this notebook, we use a another environment that must be linked first.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are two ways to start the workshop environment"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**1. Centrally from the project folder at ZIH (default):**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is the fast way."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!cd .. && sh activate_topic_env.sh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**2. Locally, from the users home folder at ZIH (currently disabled):**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is slower, since the environment will need to be installed first."
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "!cd .. && sh make_topic_env.sh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-warning\" role=\"alert\" style=\"color: black;\">\n",
    "    <strong>Afterwards, select \"topic_env\" (or \"topic_env_user\") Kernel on the top-right corner.</strong>\n",
    "    </div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\" role=\"alert\" style=\"color: black;\">\n",
    "    <div style=\"width:500px\">\n",
    "        <p><b>Important:</b></p>\n",
    "       <ul>\n",
    "           <li>In this notebook we will be working with RAW Instagram data since the method approached hasn't been applied on the HLL datastructure</li>\n",
    "           <li><strong>Please:</strong></li>\n",
    "           <ul>\n",
    "               <li><u>Do not share the original data</u></li>\n",
    "                <li>Remove original data after the workshop</li>\n",
    "           </ul>\n",
    "        </ul>\n",
    "    </div>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction: Social Media & Topic-based text classification\n",
    "\n",
    "\n",
    "The content social media users share on different platforms is extremely diverse encompassing a very wide range of topics including valuable information related to the way people perceive, relate to and use different environments. In order to harness these large volumes of data, specific tools and techniques to organize, search and understand these vast quantities of information are needed.</br>\n",
    "    \n",
    "Text classification is a Natural Language Processing task that aims at mapping documents (in our case social media posts) into a set of predefined categories. Supervised machine learning classifiers have shown great success in performing these tasks. Nevertheless, they require large volumes of labeled data for training, which are generally not available for social media data and can be very time-consuming and expensive to obtain.</br>\n",
    "\n",
    "This notebook introduces a practical and unsupervised approach (which requires no labeled data) to thematically classify social media posts into specific categories (topics) simply described by a label. The underlying assumption of this approach is that Word Embeddings can be used to classify documents when no labeled training data is available. </br>\n",
    "\n",
    "The method is based on the comparison of the textual semantic similarity between the most relevant words in each social media post and a list of keywords for each targeted category reflecting its semantics field (in linguistics, a semantic field is a set of words grouped by their meaning, that refers to a specific subject). The strenght of this approach is represented by its simplicity, however, its success depends on a good definition for each topic reflected in the list of keywords.</br>\n",
    "\n",
    "### Methodology\n",
    "\n",
    "How do we make machines understand text data? It is generally known that machines are experts when dealing and working with numerical data but their performance decreases if they are fed raw text data.\n",
    "\n",
    "The idea is to create numerical representation of words that capture their meanings, semantic relationships and the different contexts they are used in. For the coversion of raw text into numbers, there are a few options out there. The simplest methodology when dealing with text is to create a word frequency matrix that simply counts the occurrence of each word (<a href =\"https://en.wikipedia.org/wiki/Bag-of-words_model\"> bag-of-words</a>). An enhance version of this method is to estimate the log scaled frequency of each word considering its occurrence in all documents (<a href = \"https://en.wikipedia.org/wiki/Tf%E2%80%93idf\" word embeddings>tf-idf</a>). Nevertheless, these methods capture solely frequencies of words and no contextual information or high level semantics of text. \n",
    "\n",
    "A recent advance on the field of Natural Language Processing proposed the use of <a href = \"https://en.wikipedia.org/wiki/Word_embedding\"> word embeddings</a> for the numerical representation of text. \n",
    "\n",
    "<div class=\"alert alert-success\" role=\"alert\" style=\"color: black;\">\n",
    "    <details><summary><strong>Word Embeddings</strong></summary>\n",
    "\n",
    "<p>Word embeddings are a type of word representation that allows words with similar meaning to have similar representations. Behind the approach of learning these representation lies the “distributional hypothesis” by Zellig Harris, a linguistic theory that could be summarized as: words that have similar context will have similar meanings.</p>\n",
    "<p> <a href=\"https://en.wikipedia.org/wiki/Word2vec\"> Word2Vec </a> embedding approach Word2Vec is one of the most popular word embedding models and it was introduced in two <a href=\"http://arxiv.org/abs/1301.3781\"> papers </a>  between September and October 2013, by a team of researchers at Google and is considered the state of the art. Word2Vec approach uses deep learning and neural networks-based techniques to convert words into corresponding vectors in such a way that the semantically similar vectors are close to each other in N-dimensional vector space, where N refers to the dimensions of the vector. As words that have similar neighbouring words are likely semantically similar, this means that the word2vec approach is very good at capturing semantic relationships.</p>\n",
    "<p>Word2Vec's ability to capture and maintain semantic relationships is reflected by a famous classic example where if you have a vector for the word \"King\" and you remove the vector represented by the word \"Man\" from the \"King\" and add \"Woman\" to it, you get a vector which is close to the \"Queen\" vector. This relation is commonly represented as: King - Man + Woman = Queen.</p>\n",
    "<p>Word2Vec can be used to find out the relations between words in a dataset, compute the similarity between them, or use the vector representation of those words as input for applications such as text classification.</p>\n",
    "        \n",
    "For more information on Word Embeddings this could be a starting reading material: https://ruder.io/word-embeddings-1/index.html </div>\n",
    "    \n",
    "The method adopted computes the semantic similarity between different words or group of words and determines which words are semantically related to each other and belong to the same semantic field. Furthermore, computing the distance in the vector space (cosine distance) between the centroid of the word vectors that belong to a certain topic (semantic field) and the centroid of the word vectors that compose a social media posts will allow us to verify if the textual metadata associated with the posts is related to a  specific category (Binary Classification).\n",
    "\n",
    "\n",
    "In this notebook we will implement the Word2Vec word embedding technique used for creating word vectors with Python's <a href = \"https://radimrehurek.com/gensim/index.html\"> Gensim </a> library. </br>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Preparations\n",
    "\n",
    "Load Dependencies"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import pickle\n",
    "import scipy\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import warnings\n",
    "warnings.filterwarnings('ignore')\n",
    "from IPython.display import clear_output, Markdown, display"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We're creating several output graphics and temporary files.\n",
    "\n",
    "These will be stored in the subfolder **notebooks/out/**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pathlib import Path\n",
    "OUTPUT = Path.cwd() / \"out\"\n",
    "OUTPUT.mkdir(exist_ok=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.1. Load the pre-trained Word2Vec model  and the idfscores dictionary\n",
    "<br>\n",
    "The model was trained on a corpus, that has been previously prepared by filtering, cleaning and normnalizing over 1.5 M Instagram, Flickr and Twitter posts geolocated within Dresden and Heidelberg.\n",
    "\n",
    "Parameters were chosen according to the semantic similarity performance results reported in <a href=\"http://arxiv.org/abs/1301.3781\">*Efficient Estimation of Word Representations in Vector Space* </a> - T. Mikolov et al.(2013)\n",
    "- size (vector size): 300\n",
    "- alpha (initial learning rate) : 0.025\n",
    "- window: 5\n",
    "- min_count: 5\n",
    "- min_alpha: 0.0001 Learning rate will linearly drop to min_alpha as training progresses\n",
    "- sg: 1 (SkipGram architecture - predicting context words based on current one)\n",
    "- negative (negative samples): 5 If > 0, negative sampling will be used, the int for negative specifies how many “noise words” should be drawn (usually between 5-20). If set to 0, no negative sampling is used.\n",
    "- ns_exponent = 0.75 The exponent used to shape the negative sampling distribution. A value of 1.0 samples exactly in proportion to the frequencies, 0.0 samples all words equally, while a negative value samples low-frequency words more than high-frequency words. The popular default value of 0.75 was chosen by the original Word2Vec paper.\n",
    "- iter : 15 Number of iterations (epochs) over the corpus.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%load_ext autoreload\n",
    "%autoreload 2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Prepare paths.."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "\n",
    "INPUT = Path.cwd() / \"input\"\n",
    "\n",
    "module_path = str(Path.cwd().parents[0] / \"py\")\n",
    "if module_path not in sys.path:\n",
    "    sys.path.append(module_path)\n",
    "from modules import tools"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "source = \"topic_data.zip\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Download sample data. This may take some time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "sample_url = tools.get_sample_url()\n",
    "zip_uri = f'{sample_url}/download?path=%2F&files='\n",
    "tools.get_zip_extract(\n",
    "    uri=zip_uri,\n",
    "    filename=source,\n",
    "    output_path=INPUT,\n",
    "    write_intermediate=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "load the pretrained word2vec model usign gensim's Word2Vec"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from gensim import utils\n",
    "from gensim.models import Word2Vec\n",
    "model_w2v = Word2Vec.load(\n",
    "    str(INPUT / \"word2vec.model\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the creation of the post_embedding we will need idf-score weights, which were prepared beforehand and stored in the input folder as a serialized (pickled) dictionary"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#idf-scores dictionary deserialization\n",
    "with open(INPUT / 'idf_scores_dict.pkl', 'rb') as handle:\n",
    "    idf_scores_dict = pickle.load(handle)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.2. Define the functions that will help compute the average topic and post vectors\n",
    "\n",
    "Functions to compute the average topic and post vectors"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def avg_topic_vector(lang_model, tokens_list):\n",
    "    # remove out-of-vocabulary words\n",
    "    tokens = []\n",
    "    for token in tokens_list:\n",
    "        if token in lang_model.wv.vocab:\n",
    "            tokens.append(token)\n",
    "    return np.average(lang_model[tokens], axis=0)\n",
    "\n",
    "def avg_post_vector(lang_model, tokens_list,idf):\n",
    "    # remove out-of-vocabulary words\n",
    "    tokens = []\n",
    "    weights = []\n",
    "    for token in tokens_list:\n",
    "        if token in lang_model.wv.vocab:\n",
    "            tokens.append(token)\n",
    "            tf = tokens_list.count(token)\n",
    "            tfidf= tf*idf[token]\n",
    "            weights.append(tfidf)\n",
    "    return np.average(lang_model[tokens], weights =weights, axis=0)\n",
    "\n",
    "\n",
    "def has_vector_representation(lang_model, upl):\n",
    "    \"\"\"check if at least one word of the document is in the\n",
    "    word2vec dictionary\"\"\"\n",
    "    n= len([w for w in upl if w in lang_model.wv.vocab])\n",
    "    if n>0:\n",
    "        return True\n",
    "    else:\n",
    "        return False"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Load Preprocessed Data \n",
    "\n",
    "<div class=\"alert alert-info\" role=\"alert\" style=\"color: black;\">\n",
    "To enable the topic-based text classification of the social media posts, these need to be cleaned beforehand. Therefore, in this notebook we will use a central dataset that is available as a pickled pandas dataframe in the central workshop folder. The dataset consists of 115370 Instagram posts published between 2015 and 2018 and geolocated within the city of Dresden. \n",
    "</div>\n",
    "\n",
    "The textual content of social media data has a low degree of formal semantic and syntactic accuracy. In order to provide only significant information for the text classification task to be performed, the text (post_title, post_ body and tags) needed to be preprocessed according to the following actions:\n",
    " - lowercasing\n",
    " - extract hashtags and individual words (tokenization)\n",
    " - remove mentions (@username)\n",
    " - remove punctuation\n",
    " - remove the URLs (http:\\\\ as well as www.)\n",
    " - remove html tags (<>)\n",
    " - remove digits \n",
    " - identify and select only English and German posts\n",
    " - remove stopwords (commonly used words such as “the”, “a”, “an”, “in”, etc.)\n",
    " "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "filename = \"DD_Neustadt_NormalizedInstagramPosts.pickle\"\n",
    "df = pd.read_pickle(INPUT / filename)\n",
    "print(len(df))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Topic-Based Classification of Social Media Posts\n",
    "\n",
    "### Workflow\n",
    "\n",
    "The classification of the social media posts is based on the calculation of the similarity score (**cosine similarity**) between a **topic embedding** &  the **post embeddings** and follows the workflow exposed below:\n",
    "\n",
    "1. for each label (topic) a list of relevant keywords is defined and enhanced by seeking further semantically similar words through the identification of the most similar word vectors (which are closely located in the vector space)\n",
    "\n",
    "2. a *topic embedding* will be created by averaging all the vectors representing the keywords in the list previously defined\n",
    "\n",
    "3. the vector represention of each social media post is created by averaging the weighted word embeddings of all words and the weight of a word is given by its tf-idf score. \n",
    "\n",
    "<div class=\"alert alert-success\" role=\"alert\" style=\"color: black;\">\n",
    "    <details><summary><strong>TF-IDF</strong></summary>\n",
    "Tf-idf, short for term frequency - inverse document frequency, is a statistical measure that evaluates how relevant a word is to a document in a collection of documents. This is done by multiplying two metrics: how many times a word appears in a document, and the inverse document frequency of the word across a set of documents.</br>\n",
    "The tf-idf score is: </br>\n",
    " - highest when a occurs many times within a small number of documents (thus lending high discriminating power to those documents);</br>\n",
    " - lower when the term occurs fewer times in a document, or occurs in many documents (thus offering a less pronounced relevance signal); </br>\n",
    " - lowest when the term occurs in virtually all documents.</div>\n",
    "\n",
    "\n",
    "4. the classification will follow after the calculation of a similarity score between each pair of post vector - topic vector using cosine distance and an empirically identified similarity threshold (70%) is considered decisive"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-warning\" role=\"alert\" style=\"color: black;\"> \n",
    "    <b> Step 1. Define the list of keywords that represent the topic you want to explore</b> </br>\n",
    "In order to exemplify more concretely how the algorithm works, we chose to explore the topic of <b> music festivals and concerts</b> and in the following we will identify the Instagram posts that are related to this topic following the workflow described above.\n",
    "<br>\n",
    "<br>\n",
    "Use your own input by changing the keywords in the list below and explore other topics that you are interested in. Try to input more than one keyword to \"define\" the topic as specifically as possible. \n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "topic_list = ['event','music','festival','concert']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-warning\" role=\"alert\" style=\"color: black;\"> \n",
    "    <strong>Step 2. Enhance the keywords list by finding semanically similar words </strong>\n",
    "<br>\n",
    "    <br>\n",
    "<b> gensim.models.Word2Vec.most_similar</b> finds the top-N most similar words. Positive words contribute positively towards the similarity, negative words negatively.This method computes cosine similarity between a simple mean of the projection weight vectors of the given words and the vectors for each word in the model. The method corresponds to the word-analogy and distance scripts in the original word2vec implementation.\n",
    "<br>\n",
    "    <br>\n",
    "Apply the same method to find semantically similar words for your input!\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "enhanced_list = []\n",
    "for keyword in topic_list:\n",
    "    similar_words = model_w2v.wv.most_similar(\n",
    "        positive = [keyword], topn = 50)\n",
    "    enhanced_list += ([w[0] for w in similar_words])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "some words might repeat, therefore we will save the list as a set of unique strings"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "topic_list = topic_list + enhanced_list\n",
    "topic_list = set(topic_list)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-warning\" role=\"alert\" style=\"color: black;\"> \n",
    "    <b> Step 3. Create the topic embedding </b>\n",
    "<br>\n",
    "    <br>\n",
    "\n",
    "To create the topic embedding call the previously defined avg_topic_vector function.\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "topic_embedding = avg_topic_vector(model_w2v,topic_list)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### WordCloud representing the topic selected\n",
    "\n",
    "To visualize the enhanced list of keywords representative for the chosen topic and used for the calculation of the topic embedding, we use the WordCloud library.\n",
    "<br>\n",
    "\n",
    "*Note: Some of the tags identified might refer to the city of Heidelberg since the word2vec model was trained on social media posts that were published within both Dresden and Heidelberg.*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "from wordcloud import WordCloud        \n",
    "words = ' '.join(topic_list) \n",
    "wordcloud = WordCloud(background_color=\"white\").generate(words)\n",
    "# Display the generated image:\n",
    "plt.figure(figsize = (10,10))\n",
    "plt.imshow(wordcloud, interpolation='bilinear')\n",
    "plt.axis(\"off\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-warning\" role=\"alert\" style=\"color: black;\"> \n",
    "    <b> Step 4. Create post embeddings and classify social media posts </b> \n",
    "<br>\n",
    "    <br>\n",
    "We will add 2 new columns to the dataframe \"classification\" and \"cos_dist\" to save the classified Instagram posts as a dataframe that will be pickled and store in the output folder. Accordingly, the \"classification\" column will be populated with the ones (if the post_text reflects the topic selected and the calculated cosine distance is smaller than 0.3) and zeros (if the calculated cosine distance is larger than 0.3). We store the cosine distance values in the \"cos_dist\" column for further inspections. \n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "df = df.reindex(df.columns.tolist() + ['classification','cos_dist'], axis=1)\n",
    "x = 0\n",
    "total_records = len(df)\n",
    "for index, row in df.iterrows():\n",
    "    x+=1\n",
    "    msg_text = (\n",
    "        f'Processed records: {x} ({x/(total_records/100):.2f}%). ')\n",
    "    if x % 100 == 0:\n",
    "        clear_output(wait=True)\n",
    "        print(msg_text)\n",
    "        \n",
    "    text = row['post_text'].split(' ')\n",
    "    \n",
    "    if has_vector_representation(model_w2v, text) == True:\n",
    "        #create the post embedding\n",
    "        post_embedding = avg_post_vector(model_w2v, text,idf_scores_dict)\n",
    "        cos_dist = scipy.spatial.distance.cosine(topic_embedding, post_embedding, w=None)                                              \n",
    "        if cos_dist <0.3:\n",
    "            df.at[index,'classification'] = 1\n",
    "            df.at[index,'cos_dist'] = cos_dist\n",
    "        else:\n",
    "            df.at[index,'classification'] = 0\n",
    "            df.at[index,'cos_dist'] = cos_dist                               \n",
    "            \n",
    "\n",
    "# final status\n",
    "clear_output(wait=True)\n",
    "print(msg_text)\n",
    "\n",
    "df.to_pickle(OUTPUT/ 'DD_Neustadt_ClassifiedInstagramPosts.pickle')\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df_classified = df[df['classification'] == 1]\n",
    "print (\"The algorithm identified\", len(df_classified), \"social media posts related to music events in Dresden Neustadt\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. Interactive visualization of the classified posts using bokeh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Load dependencies"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import geopandas as gp\n",
    "import holoviews as hv\n",
    "import geoviews as gv\n",
    "from cartopy import crs as ccrs\n",
    "hv.notebook_extension('bokeh')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Convert the pandas dataframe into a geopandas dataframe**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "df_classified is a subset of the original dataframe and the index values will correspond with the ones in the orginal dataframe.We will reset the index values so the first record of the subset gets the index 0."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df_classified.reset_index()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gdf = gp.GeoDataFrame(\n",
    "    df_classified, geometry=gp.points_from_xy(df_classified.longitude, df_classified.latitude))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "CRS_PROJ = \"epsg:3857\" # Web Mercator\n",
    "CRS_WGS = \"epsg:4326\" # WGS1984\n",
    "gdf.crs = CRS_WGS # Set projection\n",
    "gdf = gdf.to_crs(CRS_PROJ) # Project"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Have a look at the geodataframe"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gdf.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = gdf.loc[gdf.first_valid_index()].geometry.x\n",
    "y = gdf.loc[gdf.first_valid_index()].geometry.y\n",
    "\n",
    "margin = 1000 # meters\n",
    "bbox_bottomleft = (x - margin, y - margin)\n",
    "bbox_topright = (x + margin, y + margin)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<details><summary><strong>gdf.loc[0] ?</strong></summary>\n",
    "  <ul>  \n",
    "      <li><code>gdf.loc[0]</code> is the loc-indexer from pandas. It means: access the first record of the (Geo)DataFrame\n",
    "      <li><code>.geometry.x</code> is used to access the (projected) x coordinate geometry (point). This is only available for GeoDataFrame (geopandas)</li>\n",
    "    </ul>\n",
    "</details>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "posts_layer = gv.Points(\n",
    "    df_classified,\n",
    "    kdims=['longitude', 'latitude'],\n",
    "    vdims=['post_text'],\n",
    "    label='Instagram Post')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bokeh.models import HoverTool\n",
    "from typing import Dict, Optional\n",
    "def get_custom_tooltips(\n",
    "        items: Dict[str, str]) -> str:\n",
    "    \"\"\"Compile HoverTool tooltip formatting with items to show on hover\"\"\"\n",
    "    tooltips = \"\"\n",
    "    if items:\n",
    "        tooltips = \"\".join(\n",
    "            f'<div><span style=\"font-size: 12px;\">'\n",
    "            f'<span style=\"color: #82C3EA;\">{item}:</span> '\n",
    "            f'@{item}'\n",
    "            f'</span></div>' for item in items)\n",
    "    return tooltips"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def set_active_tool(plot, element):\n",
    "    \"\"\"Enable wheel_zoom in bokeh plot by default\"\"\"\n",
    "    plot.state.toolbar.active_scroll = plot.state.tools[0]\n",
    "\n",
    "# prepare custom HoverTool\n",
    "tooltips = get_custom_tooltips(items=['post_text'])\n",
    "hover = HoverTool(tooltips=tooltips) \n",
    "    \n",
    "gv_layers = hv.Overlay(\n",
    "    gv.tile_sources.CartoDark * \\\n",
    "    posts_layer.opts(\n",
    "        tools=['hover'],\n",
    "        size=8,\n",
    "        line_color='black',\n",
    "        line_width=0.1,\n",
    "        fill_alpha=0.8,\n",
    "        fill_color='#ccff00') \n",
    "    )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Store map as static HTML file**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gv_layers.opts(\n",
    "    projection=ccrs.GOOGLE_MERCATOR,\n",
    "    title= \"Music Festivals and Concerts in Dresden Neustadt according to Instagram Posts\",\n",
    "    responsive=True,\n",
    "    xlim=(bbox_bottomleft[0], bbox_topright[0]),\n",
    "    ylim=(bbox_bottomleft[1], bbox_topright[1]),\n",
    "    data_aspect=0.45, # maintain fixed aspect ratio during responsive resize\n",
    "    hooks=[set_active_tool])\n",
    "hv.save(\n",
    "    gv_layers, OUTPUT / f'topic_map.html', backend='bokeh')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-warning\" role=\"alert\" style=\"color: black;\">\n",
    "    <details><summary><strong>Open map in new tab</strong></summary>\n",
    "        <br>\n",
    "        <div style=\"width:500px\">\n",
    "            In the file explorer on the left, go to notebooks/out/ and open geoviews_map.html with a right-click: Open in New Browser Tab.\n",
    "        </div>\n",
    "    </details>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Display in-line view of the map:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gv_layers.opts(\n",
    "    width=800,\n",
    "    height=480,\n",
    "    responsive=False,\n",
    "    hooks=[set_active_tool],\n",
    "    title= \"Music Festivals and Concerts in Dresden Neustadt according to Instagram Posts\" ,\n",
    "    projection=ccrs.GOOGLE_MERCATOR,\n",
    "    data_aspect=1,\n",
    "    xlim=(bbox_bottomleft[0], bbox_topright[0]),\n",
    "    ylim=(bbox_bottomleft[1], bbox_topright[1])\n",
    "    )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create Notebook HTML"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!jupyter nbconvert --to html \\\n",
    "    --output-dir=./out/ ./04_topic_classification.ipynb \\\n",
    "    --template=../nbconvert.tpl \\\n",
    "    --ExtractOutputPreprocessor.enabled=False >&- 2>&-"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Clean up input folder**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tools.clean_folders(\n",
    "    [Path.cwd() / \"input\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Summary\n",
    "<div class=\"alert alert-warning\" role=\"alert\" style=\"color: black;\">\n",
    "\n",
    "<p> Text classification is a complex NLP related task and a detailed description of it is beyond the scope of this notebook. This notebook is a very brief introduction of the main NLP concepts, methods and tools utilised in topic-based text classification.  </p>\n",
    "        \n",
    "<p>The method presented was developed for classifying short text such as social media posts which raises in general a series of issues related to its unstructured and noisy nature. </p>\n",
    "\n",
    "<p> The performance evaluation of the classification revealed decent results (F1-Score > 0.6). However, as observed during the performance evaluation process, misclassification occurs mainly due to two general open issue in text mining, polysemy and synonymy. Therefore, to improve the performance of the classifier, word sense disambiguation methods need to be implemented in the algorithm and it is parth of future work</p>\n",
    "        \n",
    "</div>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
