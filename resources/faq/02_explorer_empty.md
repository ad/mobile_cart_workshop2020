# Jupyter Lab Starts, but the explorer on the left is empty

This means that the gitlab repository has not been cloned properly to your user space.

One possible cause is that the spawn-link, with pre-configured parameters, did not work.

This can happen if you manually started the server, after seeing this screen:

<img src="assets/spawn_manual_conf.png"  width="400">

## Solution

### 1. Shut down your Server ( File > Shut Down)

- to shutdown the server, click on `File / Shut Down` in Jupyter 
  and on the next page click on "Stop my Server"

<img src="assets/stop_server.png"  width="400">

### 2. Wait until the server has shut down

- this may take a couple of seconds

### 3. Go back to the [repository][repo], and click the spawn link again

- after clicking the link, the server should start without asking you for any configurations
- if you see the configuration screen again, it means the spawn link did not work
- try it again, if the issue persists, get in contact with us

[repo]: https://gitlab.vgiscience.de/ad/mobile_cart_workshop2020/