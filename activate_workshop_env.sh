#!/bin/sh
# Make central p_lv_mobicart_2021 workerhop_env
# available in user jupyter lab kernel spec
# e.g.
# /home/h5/$USER/.local/share/jupyter/kernels/workshop_env

/projects/p_lv_mobicart_2021/workshop_env/bin/python \
    -m ipykernel install \
    --user \
    --name workshop_env \
    --display-name="workshop_env"

# install jupytext in user mode
# /sw/installed/Anaconda3/2019.03/bin/python \
#     -m pip install jupytext \
#     --upgrade \
#     --user